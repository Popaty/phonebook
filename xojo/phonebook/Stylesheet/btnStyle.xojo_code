#tag WebStyle
WebStyle btnStyle
Inherits WebStyle
	#tag WebStyleStateGroup
		border-top=1px solid 000000FF
		border-left=1px solid 000000FF
		border-bottom=1px solid 000000FF
		border-right=1px solid 000000FF
		misc-background=solid FFFFFFFF
		text-color=000000FF
		corner-topleft=10px
		corner-bottomleft=10px
		corner-bottomright=10px
		corner-topright=10px
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=solid BFBFBFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
		misc-background=solid 000000FF
		text-color=FFFFFFFF
	#tag EndWebStyleStateGroup
	#tag WebStyleStateGroup
	#tag EndWebStyleStateGroup
End WebStyle btnStyle
#tag EndWebStyle

