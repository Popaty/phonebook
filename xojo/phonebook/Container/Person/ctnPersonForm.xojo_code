#tag WebPage
Begin WebContainer ctnPersonForm
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   662
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   False
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   712
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebTextField fldIdUser
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   20
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbIdUser
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "รหัสพนักงาน"
      TextAlign       =   0
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbUserName
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   4
      Text            =   "ชื่อ - นามสกุล"
      TextAlign       =   0
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbNikName
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   6
      Text            =   "ชื่อเล่น"
      TextAlign       =   0
      Top             =   54
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbUserTel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   14
      Text            =   "เบอร์ภายใน"
      TextAlign       =   0
      Top             =   122
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbFloor
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   12
      Text            =   "ชั้น"
      TextAlign       =   0
      Top             =   122
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbDeptName1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   2
      Text            =   "หน่วยงาน"
      TextAlign       =   0
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbMobile
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   16
      Text            =   "เบอร์มือถือ"
      TextAlign       =   0
      Top             =   156
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbMobile2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   18
      Text            =   "เบอร์มือถือ 2"
      TextAlign       =   0
      Top             =   156
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbFax
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   24
      Text            =   "เบอร์แฟกซ์"
      TextAlign       =   0
      Top             =   224
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbMemo
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   28
      Text            =   "memo"
      TextAlign       =   0
      Top             =   258
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbEmailExt
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   22
      Text            =   "อีเมล์ other"
      TextAlign       =   0
      Top             =   190
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbUserMail
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   20
      Text            =   "อีเมล์ cccthai"
      TextAlign       =   0
      Top             =   190
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbStatusiName
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   26
      Text            =   "สถานะ"
      TextAlign       =   0
      Top             =   224
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldUserName
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "2023950335"
      TabOrder        =   5
      Text            =   ""
      TextAlign       =   0
      Top             =   54
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldNikName
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   7
      Text            =   ""
      TextAlign       =   0
      Top             =   54
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldFloor
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "2023950335"
      TabOrder        =   13
      Text            =   ""
      TextAlign       =   0
      Top             =   122
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldMobile
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   17
      Text            =   ""
      TextAlign       =   0
      Top             =   156
      Type            =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldMobile2
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LimitText       =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   19
      Text            =   ""
      TextAlign       =   0
      Top             =   156
      Type            =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldFax
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   25
      Text            =   ""
      TextAlign       =   0
      Top             =   224
      Type            =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldUserMail
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   21
      Text            =   ""
      TextAlign       =   0
      Top             =   190
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldEmailExt
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   23
      Text            =   ""
      TextAlign       =   0
      Top             =   190
      Type            =   2
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextArea fldMemo
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   230
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      ReadOnly        =   False
      Scope           =   0
      ScrollPosition  =   0
      Style           =   "1168766975"
      TabOrder        =   29
      Text            =   ""
      TextAlign       =   0
      Top             =   258
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldUserTel
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   15
      Text            =   ""
      TextAlign       =   0
      Top             =   122
      Type            =   4
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbIdUser1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   8
      Text            =   "รหัสผ่าน"
      TextAlign       =   0
      Top             =   88
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldPass
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   124
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   9
      Text            =   ""
      TextAlign       =   0
      Top             =   88
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbIdUser2
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   10
      Text            =   "ยืนยันรหัสผ่าน"
      TextAlign       =   0
      Top             =   88
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField fldConpass
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "1168766975"
      TabOrder        =   11
      Text            =   ""
      TextAlign       =   0
      Top             =   88
      Type            =   1
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel lbMemo1
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   366
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   30
      Text            =   "รูปภาพ"
      TextAlign       =   0
      Top             =   258
      VerticalCenter  =   0
      Visible         =   True
      Width           =   92
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView imgPerson
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   223
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Picture         =   0
      ProtectImage    =   True
      Scope           =   0
      Style           =   "-1"
      TabOrder        =   -1
      Top             =   258
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnDelete
      AutoDisable     =   False
      Caption         =   "ลบ"
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   592
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "-1"
      TabOrder        =   33
      Top             =   562
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin ctnDepartmentSelect cDepartmentSelect
      Cursor          =   0
      Enabled         =   True
      Height          =   24
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      ScrollbarsVisible=   0
      Style           =   "-1"
      TabOrder        =   3
      Top             =   20
      VerticalCenter  =   0
      Visible         =   True
      Width           =   218
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _ShownEventFired=   False
      _VerticalPercent=   0.0
   End
   Begin WebRadioGroup rdoStatusi
      ColumnCount     =   2
      Cursor          =   1
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "`First`,`True`,``,`True`,`True`	`Untitled`,`True`,``,`False`,`True`"
      Left            =   470
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      RowCount        =   1
      Scope           =   0
      Style           =   "-1"
      TabOrder        =   27
      Top             =   224
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebFileUploader ImageUploader
      AlternateRowColor=   &cEDF3FE00
      Cursor          =   0
      Enabled         =   True
      FileCount       =   0
      Height          =   50
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      Limit           =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   2
      Style           =   "-1"
      TabOrder        =   -1
      Top             =   500
      VerticalCenter  =   0
      Visible         =   True
      Width           =   222
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebButton btnBrowse1
      AutoDisable     =   False
      Caption         =   "Upload..."
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   470
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   32
      Top             =   562
      VerticalCenter  =   0
      Visible         =   True
      Width           =   110
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Open()
		  Self.cDepartmentSelect.LoadList("")
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Function BindData() As JSONItem
		  Dim formData As New JSONItem
		  // formData.Value("deptName") = Self.fldDeptName.Text
		  formData.Value("id") = Self.personId
		  formData.Value("pass") = Self.fldPass.Text.Trim
		  formData.Value("conpass") = Self.fldConpass.Text.Trim
		  formData.Value("userName") = Self.fldUserName.Text
		  formData.Value("nikName") = Self.fldNikName.Text
		  formData.Value("floor") = Self.fldFloor.Text
		  formData.Value("userTel") = Self.fldUserTel.Text
		  formData.Value("mobile") = Self.fldMobile.Text
		  formData.Value("mobile2") = Self.fldMobile2.Text
		  formData.Value("fax") = Self.fldFax.Text
		  formData.Value("userMail") = Self.fldUserMail.Text
		  formData.Value("emailExt") = Self.fldEmailExt.Text
		  // formData.Value("pic") = ""
		  formData.Value("memo") = Self.fldMemo.Text
		  If Self.rdoStatusi.CellSelected(0, 0) Then
		    formData.Value("statusi") = 1
		  Else
		    formData.Value("statusi") = 2
		  End If
		  formData.Value("status1") = "USER"
		  formData.Value("userDepart") = Self.cDepartmentSelect.GetValue
		  'MsgBox formData.ToString
		  Return formData
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearForm()
		  Dim blankPic As Picture
		  Self.personId = 0
		  Self.fldIdUser.Text = ""
		  Self.fldUserName.Text = ""
		  Self.fldNikName.Text = ""
		  Self.cDepartmentSelect.SetValue(0)
		  Self.fldFloor.Text = ""
		  Self.fldUserTel.Text = ""
		  Self.fldMobile.Text = ""
		  Self.fldMobile2.Text = ""
		  Self.fldFax.Text = ""
		  Self.fldUserMail.Text = ""
		  Self.fldEmailExt.Text = ""
		  Self.fldMemo.Text = ""
		  Self.imgPerson.Picture = blankPic
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Delete()
		  Dim fInstance As New Repo.WS.Person
		  Dim result As New JSONItem
		  
		  result = fInstance.Delete(Self.personId)
		  
		  If result.Value("status") Then
		    MsgBox "ลบข้อมูลสำเร็จ"
		    PersonListWindows.cPersonList.LoadList
		  Else
		    MsgBox result.Value("message")
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Save()
		  If Self.Validate Then
		    Dim fInstance As New Repo.WS.Person
		    Dim result As New JSONItem
		    If Self.personId = 0 Then
		      result = fInstance.Save(BindData)
		    Else
		      result = fInstance.Update(BindData)
		    End If
		    If result.Value("status") Then
		      MsgBox "บันทึกสำเร็จ"
		      Self.ClearForm
		      PersonListWindows.Show
		    Else
		      MsgBox result.Value("message")
		    End
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub SetValue(id As Integer)
		  Self.personId = id
		  Self.rdoStatusi.CellCaption(0,0) = "ปกติ"
		  Self.rdoStatusi.CellCaption(0,1) = "ลาออก"
		  If id > 0 Then
		    Dim uInstant As New Repo.WS.Person
		    Dim result As New JSONItem
		    result = uInstant.GetByID(id)
		    If result.Value("status") Then
		      Dim formData As JSONItem = result.Value("data")
		      Self.fldIdUser.Text = formData.Value("idUser")
		      Self.fldUserName.Text = formData.Value("userName")
		      Self.fldNikName.Text = formData.Value("nikName")
		      Self.cDepartmentSelect.SetValue(formData.Value("deptId").IntegerValue)
		      Self.fldFloor.Text = formData.Value("floor")
		      Self.fldUserTel.Text = formData.Value("userTel")
		      Self.fldMobile.Text = formData.Value("mobile")
		      Self.fldMobile2.Text = formData.Value("mobile2")
		      Self.fldFax.Text = formData.Value("fax")
		      Self.fldUserMail.Text = formData.Value("userMail")
		      Self.fldEmailExt.Text = formData.Value("emailExt")
		      Self.fldMemo.Text = formData.Value("memo")
		      If formData.Value("statusi").IntegerValue = 1 Then
		        Self.rdoStatusi.CellValue(0,0) = True
		      Else
		        Self.rdoStatusi.CellValue(0,1) = True
		      End If
		      // Self.fldStatusiName.Text = formData.Value("statusiName")
		      // Self.fldStatus1.Text = formData.Value("status1")
		      PersonFormWindows.cContent.setTitle("แก้ไข "+formData.Value("userName"))
		      If formData.Value("pic") <> "" Then
		        Self.imgPerson.Picture = Utils.ImgTools.ThumbnailPicture(Utils.ImgTools.StringToPicture(formData.Value("pic")),Self.imgPerson.Width,Self.imgPerson.Height)
		      End If
		    Else
		      MsgBox result.Value("message")
		    End If
		  Else
		    PersonFormWindows.cContent.setTitle("เพิ่มข้อมูลใหม่")
		    ClearForm
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Validate() As Boolean
		  Dim result As Boolean = True
		  Dim message As String
		  If Self.fldPass.Text.Trim <> "" Then
		    If Self.fldPass.Text.Trim <> Self.fldConpass.Text.Trim Then
		      result = False
		      message = "รหัสผ่าน กับ ยืนยันรหัสผ่าน ไม่ตรงกัน"+EndOfLine
		    End If
		  End If
		  If Self.fldUserName.Text.Trim = "" Then
		    result = False
		    message = message+"ชื่อ - นามสกุล ห้ามว่าง"+EndOfLine
		  End If
		  If Self.fldFloor.Text.Trim = "" Then
		    result = False
		    message = message+"ชั้นห้ามว่าง"
		  End If
		  If message <> "" Then
		    MsgBox message
		  End If
		  Return result
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		delPicture As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			ImageProfile
		#tag EndNote
		Private ImageExtString As String
	#tag EndProperty

	#tag Property, Flags = &h21
		#tag Note
			ImageProfile
		#tag EndNote
		Private ImageProfileString As String
	#tag EndProperty

	#tag Property, Flags = &h0
		personId As Integer = 0
	#tag EndProperty

	#tag Property, Flags = &h0
		picFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		ProfilePhoto As String
	#tag EndProperty


#tag EndWindowCode

#tag Events btnDelete
	#tag Event
		Sub Action()
		  Dim blankPic As Picture
		  Self.imgPerson.Picture = blankPic
		  Self.delPicture = True
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ImageUploader
	#tag Event
		Sub FileAdded(Filename As String)
		  // Remove any files that do not end in .jpg
		  If Right(Filename, 4) <> ".jpg" and Right(Filename, 5) <> ".jpeg"  Then
		    // MsgBox("Please choose only JPG files")
		    MsgBox("กรุณาอัพโหลดเฉพาะไฟล์นามสกุล .jpg เท่านั้น")
		    Me.RemoveFileAtIndex(Me.FileCount - 1)
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Sub UploadBegin(FileCount As Integer)
		  #Pragma Unused FileCount
		  // This event fires as the upload begins. In this example
		  // this information is not needed.
		End Sub
	#tag EndEvent
	#tag Event
		Sub UploadComplete(Files() As WebUploadedFile)
		  Dim uploadFolder As New FolderItem
		  #If TargetWin32
		    //Windows-specific code here
		    uploadFolder = uploadFolder.Parent.Parent.Child("UploadResource")
		    if uploadFolder.Exists = false then
		      uploadFolder.CreateAsFolder
		    end if
		    'MsgBox Str("Win : "+uploadFolder.AbsolutePath)
		  #ElseIf TargetMacOS
		    //OS X-specific code goes here
		    uploadFolder = uploadFolder.Parent.Parent.Child("UploadResource")
		    if uploadFolder.Exists = false then
		      uploadFolder.CreateAsFolder
		    end if
		    'MsgBox Str("Mac : "+uploadFolder.AbsolutePath)
		  #ElseIf TargetLinux Then
		    //Linux-specific code goes here
		    uploadFolder = uploadFolder.Child("UploadResource")
		    if uploadFolder.Exists = false then
		      uploadFolder.CreateAsFolder
		    end if
		  #EndIf
		  
		  If Not uploadFolder.Exists Then
		    #If DebugBuild Then
		      uploadFolder.CreateAsFolder
		    #Else
		      MsgBox("Upload failed. Upload folder does not exist: " + uploadFolder.NativePath)
		      Return
		    #Endif
		  End If
		  
		  Dim uploadedPicture As Picture
		  Dim savePicture As FolderItem
		  
		  For Each uFile As WebUploadedFile In files
		    Try
		      // Attempt to verify that the file is actually a picture.
		      // If it is not a picture, this will raise an exception and the file
		      // is skipped.
		      If uFile.File <> Nil Then
		        // The file is in the temp folder on disk, so we can load it from there
		        // to save memory.
		        uploadedPicture = Picture.Open(uFile.File)
		      Else
		        // The file is in memory.
		        uploadedPicture = Picture.FromData(uFile.Data)
		      End If
		      
		      // Now save the file to disk in our upload folder
		      
		      //Create name for Upload Image
		      Dim today As New Date
		      Dim todayIs As String
		      todayIs = today.Day.ToText+"_"+today.Month.ToText+"_"+today.Year.ToText+"_"+today.Hour.ToText+"_"+today.Minute.ToText+"_"+today.Second.ToText
		      Dim profilePicure As String
		      if Right(uFile.Name, 5) = ".jpeg" then
		        profilePicure = "imgProfile_"+todayIs+ Right(uFile.Name, 5)
		      else
		        profilePicure = "imgProfile_"+todayIs+ Right(uFile.Name, 4)
		      end
		      savePicture = uploadFolder.Child(profilePicure)
		      uFile.Save(savePicture)
		      MsgBox "อัพโหลดรูปถ่าย "+profilePicure+" สำเร็จ"
		      
		      //Set ImageName to container property
		      Self.ProfilePhoto = profilePicure
		      
		      //Show image upload to image viewer
		      Dim pic As Picture
		      pic = Picture.Open(savePicture)
		      Self.picFile = savePicture
		      Self.imgPerson.Picture = Utils.ImgTools.ThumbnailPicture(pic,Self.imgPerson.Width,Self.imgPerson.Height)
		      
		    Catch err As UnsupportedFormatException
		      // not a picture, so skip it
		      Continue
		      
		    Catch err As IOException
		      MsgBox("Upload failed: Insufficient permissions to save files to " + uploadFolder.NativePath)
		      Exit For
		    End Try
		  Next
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events btnBrowse1
	#tag Event
		Sub Action()
		  Self.ImageUploader.Upload
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="delPicture"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="personId"
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ProfilePhoto"
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
