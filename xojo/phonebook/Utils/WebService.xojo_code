#tag Module
Protected Module WebService
	#tag Method, Flags = &h1
		Protected Sub DeleteImageFile(id As String)
		  Dim hSocket as new HTTPSocket
		  Dim HTMLForm As New Dictionary  
		  HTMLForm.Value("pic") = "delete"
		  Utils.WebService.SetFormData(hSocket, HTMLForm, "")
		  Dim urlUpload as String
		  urlUpload = App.kURL +"api/person/"+id+"/upload"
		  hSocket.SetRequestHeader("Authorization", Utils.gToken)
		  hSocket.Post(urlUpload)
		  
		  If hSocket.HTTPStatusCode = 200 or hSocket.HTTPStatusCode = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(hSocket.HTTPStatusCode)
		    If reqData.Value("status") Then
		      reqData.Value("status") = True
		    End If
		    System.DebugLog reqData.ToString
		  End
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function InfoResponseCode(responseCode As Integer) As JSONItem
		  Dim result As New JSONItem
		  Select Case responseCode
		    //ทดสอบเพิ่ม case responsecode return กับมาเป็น 0
		  Case 0
		    result.Value("status") = true
		  Case 200
		    result.Value("status") = true
		  Case 400
		    result.Value("status") = false
		    result.Value("message") = "Bad Request."
		  Case 401
		    result.Value("status") = false
		    result.Value("message") = "Username or Password wrong."
		  Case 402
		    result.Value("status") = false
		    result.Value("message") = "Username or Password wrong."
		  Case 403
		    result.Value("status") = false
		    result.Value("message") = "Sorry, you're not authorized."
		  Case 302
		    result.Value("status") = false
		    result.Value("message") = "Wrong Token."
		  End Select
		  Return result
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function IsConnect() As Boolean
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURL 
		  c.OptionHeader = False
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  If response = 0 Then
		    Return True
		  Else
		    Return False
		  End
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub SetFormData(sock as HTTPSocket, FormData as Dictionary, Boundary as String)
		  If Boundary.Trim = "" Then  
		    Boundary = "--" + Right(EncodeHex(MD5(Str(Microseconds))), 24) + "-bOuNdArY"  
		  End If  
		  
		  Static CRLF As String = EndOfLine.Windows  
		  Dim data As New MemoryBlock(0)  
		  Dim out As New BinaryStream(data)  
		  
		  For Each key As String In FormData.Keys  
		    out.Write("--" + Boundary + CRLF)  
		    If VarType(FormData.Value(Key)) = Variant.TypeString Then  
		      out.Write("Content-Disposition: form-data; name=""" + key + """" + CRLF + CRLF)  
		      out.Write(FormData.Value(key) + CRLF)  
		    ElseIf FormData.Value(Key) IsA FolderItem Then  
		      Dim file As FolderItem = FormData.Value(key)  
		      out.Write("Content-Disposition: form-data; name=""" + key + """; filename=""" + File.Name + """" + CRLF)  
		      out.Write("Content-Type: application/octet-stream" + CRLF + CRLF) ' replace with actual MIME Type  
		      Dim bs As BinaryStream = BinaryStream.Open(File)  
		      out.Write(bs.Read(bs.Length) + CRLF)  
		      bs.Close  
		    End If  
		  Next  
		  out.Write("--" + Boundary + "--" + CRLF)  
		  out.Close  
		  #If RBVersion > 2012 Then  
		    sock.SetRequestContent(data, "multipart/form-data; boundary=" + Boundary)  
		  #else  
		    sock.SetPostContent(data, "multipart/form-data; boundary=" + Boundary)  
		  #endif  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub UploadImageFile(file as FolderItem, id as String)
		  If file<> Nil Then
		    Dim hSocket as new HTTPSocket
		    Dim HTMLForm As New Dictionary  
		    HTMLForm.Value("pic") = file
		    Utils.WebService.SetFormData(hSocket, HTMLForm, "")
		    Dim urlUpload as String
		    urlUpload = App.kURL +"api/person/"+id+"/upload"
		    hSocket.SetRequestHeader("Authorization", Utils.gToken)
		    hSocket.Post(urlUpload)
		    
		    If hSocket.HTTPStatusCode = 200 or hSocket.HTTPStatusCode = 0 Then
		      Dim reqData As New JSONItem
		      reqData = Utils.WebService.InfoResponseCode(hSocket.HTTPStatusCode)
		      If reqData.Value("status") Then
		        reqData.Value("status") = True
		      End If
		      System.DebugLog reqData.ToString
		    End
		  Else
		  End If
		End Sub
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
