#tag Class
Protected Class Person
	#tag Method, Flags = &h0
		Function Delete(id as Integer) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectOutputData = True
		  
		  httpHeader.Append("Accept-Version: 1.0")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "DELETE"
		  
		  Dim URL As String
		  URL = App.kURL + Self.NameService +"/"+ Str(id)
		  c.OptionURL = URL
		  
		  response = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id as Integer) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURL + NameService + "/" + Str(id)
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll(searchObj As JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  Dim URL As String
		  URL = App.kURL + Self.NameService + "/list?searchText="+searchObj.Value("searchText")+"&department="+searchObj.Value("deptId")
		  
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.OptionVerbose = True
		  c.OptionURL = App.kURL + Self.NameService
		  
		  Dim httpHeader() As String
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  c.FormAdd(1,"pass", 4, formData.Value("pass"))
		  c.FormAdd(1,"conpass", 4, formData.Value("conpass"))
		  c.FormAdd(1,"userName", 4, formData.Value("userName"))
		  c.FormAdd(1,"nikName", 4, formData.Value("nikName"))
		  c.FormAdd(1,"floor", 4, formData.Value("floor"))
		  c.FormAdd(1,"userTel", 4, formData.Value("userTel"))
		  c.FormAdd(1,"mobile", 4, formData.Value("mobile"))
		  c.FormAdd(1,"mobile2", 4, formData.Value("mobile2"))
		  c.FormAdd(1,"fax", 4, formData.Value("fax"))
		  c.FormAdd(1,"userMail", 4, formData.Value("userMail"))
		  c.FormAdd(1,"emailExt", 4, formData.Value("emailExt"))
		  c.FormAdd(1,"memo", 4, formData.Value("memo"))
		  c.FormAdd(1,"statusi", 4, formData.Value("statusi"))
		  c.FormAdd(1,"status1", 4, formData.Value("status1"))
		  c.FormAdd(1,"userDepart", 4, formData.Value("userDepart"))
		  c.FormFinish
		  
		  Dim response As Integer = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      Dim getIDJson as new JSONItem
		      getIDJson = new JSONItem(c.OutputData)
		      getIDJson = getIDJson.Value("data")
		      If PersonFormWindows.cPersonForm.delPicture Then
		        Utils.WebService.DeleteImageFile(getIDJson.Value("id").StringValue)
		      End If
		      Utils.WebService.UploadImageFile(PersonFormWindows.cPersonForm.picFile,getIDJson.Value("id").StringValue)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  c.OptionHeader = False
		  // c.OptionVerbose = True
		  // c.OptionPut = True
		  
		  
		  c.OptionVerbose=true 
		  c.OptionUpload=true 
		  c.OptionPut=true 
		  c.CollectDebugData = True 
		  c.CollectOutputData = true
		  
		  
		  
		  // c.OptionCustomRequest = "PUT"
		  
		  
		  Dim httpHeader() As String
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Authorization:"+Session.gToken)
		  httpHeader.Append("Content-Type: application/x-www-form-urlencoded")
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  Dim cda As String = "&userName="+formData.Value("userName")
		  cda = cda+"&nikName="+formData.Value("nikName")
		  cda = cda+"&floor="+formData.Value("floor")
		  cda = cda+"&userTel="+formData.Value("userTel")
		  cda = cda+"&mobile="+formData.Value("mobile")
		  cda = cda+"&mobile2="+formData.Value("mobile2")
		  cda = cda+"&fax="+formData.Value("fax")
		  cda = cda+"&userMail="+formData.Value("userMail")
		  cda = cda+"&emailExt="+formData.Value("emailExt")
		  cda = cda+"&memo="+formData.Value("memo")
		  cda = cda+"&statusi="+formData.Value("statusi")
		  cda = cda+"&status1="+formData.Value("status1")
		  cda = cda+"&userDepart="+formData.Value("userDepart")
		  If formData.Value("pass") <> "" Then
		    cda = cda+"&pass="+formData.Value("pass")
		    cda = cda+"&conpass="+formData.Value("conpass")
		  End If 
		  c.inputdata=cda
		  c.OptionInFileSize=lenb(cda)
		  c.OptionURL = App.kURL + Self.NameService + "/" + formData.Value("id")
		  
		  Dim response As Integer = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		      Dim getId As New JSONItem
		      getId = reqData.Value("data")
		      If PersonFormWindows.cPersonForm.delPicture Then
		        Utils.WebService.DeleteImageFile(getId.Value("id").StringValue)
		      End If
		      Utils.WebService.UploadImageFile(PersonFormWindows.cPersonForm.picFile,getId.Value("id").StringValue)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Upload(file as FolderItem, id as String)
		  If file<> Nil Then
		    Dim MyHTTPSocket as new HTTPSocket
		    Dim HTMLForm As New Dictionary  
		    HTMLForm.Value("imageProfile") = file
		    'HTMLForm.Value("imageProfile") = "delete"
		    Utils.WebService.SetFormData(MyHTTPSocket, HTMLForm, "")
		    Dim urlUpload as String
		    urlUpload = Utils.GetServer+"personnels/"+id+"/upload"
		    MyHTTPSocket.SetRequestHeader("Authorization", Utils.gToken)
		    MyHTTPSocket.Post(urlUpload)
		    'MsgBox MyHTTPSocket.HTTPStatusCode.ToText
		    
		    If MyHTTPSocket.HTTPStatusCode = 200 or MyHTTPSocket.HTTPStatusCode = 0 Then
		      Dim reqData As New JSONItem
		      reqData = Utils.WebService.InfoResponseCode(MyHTTPSocket.HTTPStatusCode)
		      If reqData.Value("status") Then
		        reqData.Value("status") = True
		      End If
		      System.DebugLog reqData.ToString
		    End
		  Else
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "api/person"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
