#tag Class
Protected Class Department
	#tag Method, Flags = &h0
		Function Delete(id as Integer) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.CollectOutputData = True
		  
		  httpHeader.Append("Accept-Version: 1.0")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.OptionCustomRequest = "DELETE"
		  
		  Dim URL As String
		  URL = App.kURL + Self.NameService +"/"+ Str(id)
		  c.OptionURL = URL
		  
		  response = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GetByID(id as Integer) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  c.OptionVerbose=True
		  c.OptionURL = App.kURL + NameService + "/" + Str(id)
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function ListAll(searchTxt As String,max As Integer) As JSONItem
		  Dim c As New CURLSMBS
		  Dim response As Integer
		  Dim httpHeader() As String
		  
		  Dim URL As String
		  URL = App.kURL + Self.NameService + "/list?searchText="+searchTxt+"&max="+Str(max)
		  c.OptionVerbose=True
		  c.OptionURL = URL
		  c.OptionHeader = False
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Content-Type: application/json; charset=UTF-8")
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  c.CollectDebugData = True
		  c.CollectOutputData = True
		  response = c.Perform
		  
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Save(formData as JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.OptionVerbose = True
		  c.OptionURL = App.kURL + Self.NameService
		  
		  Dim httpHeader() As String
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Authorization:"+Session.gToken)
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  c.FormAdd(1,"deptName", 4, formData.Value("deptName"))
		  c.FormFinish
		  
		  Dim response As Integer = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function Update(formData as JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  c.OptionHeader = False
		  // c.OptionVerbose = True
		  // c.OptionPut = True
		  
		  
		  c.OptionVerbose=true 
		  c.OptionUpload=true 
		  c.OptionPut=true 
		  c.CollectDebugData = True 
		  c.CollectOutputData = true
		  
		  
		  
		  // c.OptionCustomRequest = "PUT"
		  
		  
		  Dim httpHeader() As String
		  httpHeader.Append("Accept-Version: " + Self.ApiVersion)
		  httpHeader.Append("Authorization:"+Session.gToken)
		  httpHeader.Append("Content-Type: application/x-www-form-urlencoded")
		  c.SetOptionHTTPHeader(httpHeader)
		  
		  Dim cda As String = "deptName="+formData.Value("deptName")
		  c.inputdata=cda
		  c.OptionInFileSize=lenb(cda)
		  c.OptionURL = App.kURL + Self.NameService + "/" + formData.Value("id")
		  
		  Dim response As Integer = c.Perform
		  If response = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      reqData = New JSONItem(c.OutputData)
		    End If
		    Return reqData
		  End
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "api/department"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
