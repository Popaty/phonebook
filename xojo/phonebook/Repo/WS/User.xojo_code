#tag Class
Protected Class User
	#tag Method, Flags = &h0
		Function Login(formData As JSONItem) As JSONItem
		  Dim c As New CURLSMBS
		  Dim data() As String
		  
		  data.Append("Accept-Version: 1.0")
		  data.Append("Content-Type: application/json")
		  data.Append("Content-Length: " + Str(LenB(formData.ToString)))
		  
		  c.OptionVerbose=True
		  c.OptionHeader = False
		  c.OptionPost = True
		  c.SetOptionHTTPHeader(data)
		  c.OptionPostFields = formData.ToString
		  c.OptionURL = App.kURL+"api/login"
		  If c.Perform = 0 Then
		    Dim reqData As New JSONItem
		    reqData = Utils.WebService.InfoResponseCode(c.GetInfoResponseCode)
		    If reqData.Value("status") Then
		      Dim loginData As New JSONItem
		      loginData = New JSONItem(c.OutputData)
		      reqData.Value("username") = loginData.Value("username")
		      reqData.Value("access_token") = loginData.Value("access_token")
		      reqData.Value("role") = "USER"
		      Dim listRole As JSONItem = loginData.Value("roles")
		      For i As Integer = 0 To listRole.Count - 1
		        If listRole(0) = "ROLE_ADMIN" Then
		          reqData.Value("role") = "ADMIN"
		        End If
		      Next
		    End If
		    Return reqData
		  End If
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private ApiVersion As String = "1.0"
	#tag EndProperty

	#tag Property, Flags = &h21
		Private NameService As String = "users"
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
