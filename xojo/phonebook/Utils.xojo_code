#tag Module
Protected Module Utils
	#tag Method, Flags = &h0
		Function GetServer() As String
		  Dim ServerUrl As String
		  ServerUrl = Utils.gServer
		  If Utils.gPort <> "" Then
		    ServerUrl = ServerUrl+":"+Utils.gPort+"/"
		  End If
		  Return ServerUrl
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub gLogError(msg As String)
		  MsgBox msg
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub LoadConfig()
		  Dim xml As New XmlDocument
		  Dim f As FolderItem = GetFolderItem("Config.xml")
		  
		  If f.Exists Then
		    xml.LoadXml(f)
		    Dim root As XmlNode
		    root = xml.DocumentElement
		    Dim serverNode As XmlNode
		    
		    serverNode = root.FirstChild
		    Utils.gServer = Str(serverNode.GetAttribute("Server"))
		    Utils.gPort = Str(serverNode.GetAttribute("Port"))
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function MapJSONItem(data As JSONItem,nData As JSONItem) As JSONItem
		  For Each fieldName As String In nData.Names()
		    data.Value(fieldName) = nData.Value(fieldName)
		  Next
		  return data
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		gPort As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gServer As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gToken As String
	#tag EndProperty

	#tag Property, Flags = &h0
		gUsername As String
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="gPort"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gServer"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gToken"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="gUsername"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
