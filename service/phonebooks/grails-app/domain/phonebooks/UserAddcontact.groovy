package phonebooks

class UserAddcontact {

    Integer id
    Integer idUser
    String pass
    String conpass
    String userName
    String nikName
    String floor
    String userTel
    String mobile
    String mobile2
    String fax
    String userMail
    String emailExt
    String pic
    String memo
    Integer statusi
    String status1

    static belongsTo = [userDepart:Department]

    static constraints = {
        idUser nullable: false
        pass nullable: true,blank: true
        conpass nullable: true,blank: true
        userName nullable: true,blank: true
        nikName nullable: true,blank: true
        userDepart nullable: true
        floor nullable: true,blank: true
        userTel nullable: true,blank: true
        mobile nullable: true,blank: true
        mobile2 nullable: true,blank: true
        fax nullable: true,blank: true
        userMail nullable: true,blank: true
        emailExt nullable: true,blank: true
        pic nullable: true,blank: true
        memo nullable: true,blank: true
        statusi nullable: true
        status1 nullable: true,blank: true
    }

    static mapping = {
        comment "Person"
        id generator:'native', params:[sequence:'user_addcontact_id_user_seq']
        idUser comment:"รหัสพนักงาน"
        pass comment:"รหัสผ่าน"
        conpass comment:"ยืนยันรหัสผ่าน"
        userName comment:"ชื่อ"
        nikName comment:"ชื่อเล่น"
        floor comment:"ชั้น"
        userTel comment:"เบอร์ภายใน"
        mobile comment:"เบอร์โทรศัพท์มือถือ"
        mobile2 comment:"เบอร์โทรศัพท์มือถือ 2"
        fax comment:"เบอร์โทรสาร"
        userMail comment:"อีเมล์ cccthai"
        emailExt comment:"อีเมล์อื่นๆ"
        memo comment:"memo",type: "text"
        statusi comment:"สถานะ",defaultValue:"1"
        status1 comment:"สิทธิ์การเข้าใช้"
        version false
    }

    def beforeValidate() {
        if(!this.idUser)
        {
            def lastPerson = UserAddcontact.last()
            this.idUser = lastPerson.getIdUser()+1
        }
    }

    def getDepartmentName() { this.getUserDepart()?this.getUserDepart().getDeptName():"" }

    def getStatusiName() {
        String result = 'ปกติ'
        if (this.statusi == 2)
        {
            result = 'ลาออก'
        }
        return result
    }
}
