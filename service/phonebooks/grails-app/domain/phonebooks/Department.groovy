package phonebooks

class Department {

    static auditable = true
    String deptName

    static constraints = {
        deptName nullable: false,blank: false
    }

    static mapping = {
        comment "สังกัด"
        id generator:'native',column:'dept_id', params:[sequence:'dept_id_seq']
        deptName comment:"ชื่อสังกัด"
        version false
    }

    public getPeopleCount()
    {
        return UserAddcontact.countByUserDepart(this)
    }
}
