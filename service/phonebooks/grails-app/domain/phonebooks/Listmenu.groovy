package phonebooks

class Listmenu {

    static auditable = true
    String status

    static constraints = {
        status nullable: false,blank: false
    }

    static mapping = {
        comment "Listmenu"
        id generator:'native',column:'id_s', params:[sequence:'listmenu_id_s_seq']
        status comment:"ชื่อ status"
        version false
    }
}
