package phonebooks

class Module {

    static auditable = true
    String module

    static constraints = {
        module nullable: false,blank: false
    }

    static mapping = {
        comment "Module"
        id generator:'native',column:'id_module', params:[sequence:'module_id_seq']
        module comment:"ชื่อ module"
        version false
    }
}
