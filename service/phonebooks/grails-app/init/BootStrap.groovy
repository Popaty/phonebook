import grails.converters.JSON
import phonebooks.Requestmap
import phonebooks.Role
import phonebooks.UserRole
import phonebooks.Users

class BootStrap {

    def init = { servletContext ->
        JSON.registerObjectMarshaller(Date) {
            return it?.format("dd/MM/yyyy")
        }
        if (!Users.last()) {
            def saAdmin = new Role(authority: "ROLE_SAADMIN").save()
            def rAdmin = new Role(authority: "ROLE_ADMIN").save()
            def rUser = new Role(authority: "ROLE_USER").save()
            def uAdmin = new Users(username: "admin",password: "password",firstName: "admin",lastName: "admin",email: "admin@gmail.com").save()
            def uUser = new Users(username: "user",password: "password",firstName: "user",lastName: "user",email: "user@gmail.com").save()
            new UserRole(user: uAdmin,role: rAdmin).save()
            new UserRole(user: uUser,role: rUser).save()
        }
        if (!Requestmap.last()) {
            for (String url in [
                    '/', '/error', '/index', '/index.gsp', '/**/favicon.ico', '/shutdown',
                    '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
                    '/api/login', '/api/login.*', '/api/login/*',
                    '/logout', '/logout.*', '/logout/*']) {
                new Requestmap(url: url, configAttribute: 'permitAll').save()
            }
            def csvFile = new File("grails-app/assets/csv/ServiceList.csv")
            csvFile.splitEachLine(';') { fields ->
                def hMethod = fields[1] ?: ""
                def cAttr = fields[2] ?: ""
                if (fields[0].trim() != "" && hMethod.trim() != "" && cAttr.trim() != "") {
                    if (fields[0] == "/saAdmins" && hMethod == "POST") {
                        cAttr = "permitAll"
                    }
                    new Requestmap(url: fields[0], httpMethod: hMethod, configAttribute: cAttr).save()
                }
            }
        }
    }
    def destroy = {
    }
}
