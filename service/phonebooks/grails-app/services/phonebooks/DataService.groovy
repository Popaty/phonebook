package phonebooks

import grails.transaction.Transactional

@Transactional
class DataService {

    def checkKey(dataInput,String fieldName) {
        def result = false
        if (dataInput.containsKey(fieldName) && dataInput[fieldName] != "") {
            result = true
        }
        return result
    }

}
