package phonebooks

import grails.transaction.Transactional
import grails.web.context.ServletContextHolder
import sun.misc.BASE64Encoder

import javax.imageio.ImageIO
import java.awt.image.BufferedImage

@Transactional
class UploadService {

    def servletContext = ServletContextHolder.servletContext
    def uploadResource = servletContext.getRealPath("/uploadresource")

    def getPathResource()
    {
        return uploadResource
    }

    def uploadFile(request,String fieldName,String pathFile,String prefixName) {
        // Create a File object representing the folder
        def (folder,fileName) = [new File(uploadResource+"${pathFile}"),""]
        // If it doesn't exist
        if( !folder.exists() ) {
            // Create all folders up-to and including B
            folder.mkdirs()
        }
        def f = request.getFile(fieldName)
        if(!f.empty){
            def oriFileName = f.originalFilename.split("\\.")
            fileName = "${prefixName}_${oriFileName[0].replaceAll(" ","")}_${UUID.randomUUID().toString().replaceAll("-","")}.${oriFileName[1]}"
            def fullPath = uploadResource+"${pathFile}/${fileName}"
            f.transferTo(new File(fullPath))
        }
        return fileName
    }

    def deleteFile(String fieldName,String pathFile)
    {
        boolean fileSuccessfullyDeleted = new File("${uploadResource}${pathFile}/${fieldName}").delete()
    }

    def readImage(String imageName,String filePath) {
        String imageString = ""
        def getContentType = org.apache.commons.lang.StringUtils.substringAfter(imageName,".")
        def File = new File(uploadResource+"/"+filePath+"/" + imageName)
        if(File.exists())
        {
            BufferedImage img = ImageIO.read(new File(uploadResource+"/"+filePath+"/" + imageName));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(img, getContentType , bos);
            byte[] imageBytes = bos.toByteArray();
            BASE64Encoder encoder = new BASE64Encoder();
            imageString = encoder.encode(imageBytes);
            bos.close();
        }
        return imageString
    }
}
