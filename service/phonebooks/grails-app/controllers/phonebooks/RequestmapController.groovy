package phonebooks

import com.wordnik.swagger.annotations.Api
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.sql.Sql

@Api(
        value = 'requestmaps',
        description = 'Requestmaps API ( Requestmaps สำหรับ permission ) V1',
        position = 4,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class RequestmapController {

    def springSecurityService
    def dataSource
    def errorMessageService

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]

    def list() {
        def searchTxt = params["searchTxt"]?:""
        String ignoreUrl = $/[
                '/', '/error', '/index', '/index.gsp', '/**/favicon.ico', '/shutdown',
                '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
                '/login', '/login.*', '/login/*','/logout', '/logout.*','/logout/*',
                '/saAdmins','/saAdmins/*','/login','/login/*',
                '/users/list','/users', '/users/*',
                '/roles/list','/roles','/roles/*',
                '/requestmaps/list','/requestmaps/loadDefault','/requestmaps','/requestmaps/*'
                ]/$
        String query = $/
            SELECT id,url,config_attribute,http_method FROM requestmap WHERE NOT (
            url = ANY(ARRAY${ignoreUrl})) AND LOWER(url) like LOWER('%${searchTxt}%')
        /$
        def db = new Sql(dataSource)
        def reList = db.rows(query)
        def result = [status:true]
        result["data"] = []
        for (reItem in reList)
        {
            def newItem = [id:reItem["id"]]
            newItem["url"] = reItem["url"]
            newItem["configAttribute"] = reItem["config_attribute"]
            newItem["httpMethod"] = reItem["http_method"]
            result["data"].push(newItem)
        }
        render result as JSON
    }

    def loadDefault() {
        String query = $/
            DELETE FROM requestmap
        /$
        def (db, result) = [new Sql(dataSource), [status: true]]
        db.execute(query)

        for (String url in [
                '/', '/error', '/index', '/index.gsp', '/**/favicon.ico', '/shutdown',
                '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
                '/login', '/login.*', '/login/*',
                '/logout', '/logout.*', '/logout/*', '/login', '/login/*']) {
            new Requestmap(url: url, configAttribute: 'permitAll').save()
        }
        def csvFile = new File("grails-app/assets/csv/ServiceList.csv")
        csvFile.splitEachLine(';') { fields ->
            def hMethod = fields[1]?:""
            def cAttr = fields[2]?:""
            if (fields[0].trim() != "" && hMethod.trim() != "" && cAttr.trim() != "")
            {
                new Requestmap(url: fields[0], httpMethod: hMethod, configAttribute: cAttr).save()
            }
        }
        springSecurityService.clearCachedRequestmaps()
        render result as JSON
    }

    @Transactional
    def create() {
        def (result,dataJson) = [["status":true],request.JSON]
        dataJson.remove("id")
        if (dataJson["httpMethod"] == "")
        {
            dataJson.remove("httpMethod")
        }
        def dataObj = new Requestmap(dataJson)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
            springSecurityService.clearCachedRequestmaps()
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Requestmap \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def update() {
        def (result, dataObj,dataJson) = [["status": true], null,request.JSON]
        if (dataJson["httpMethod"] == "")
        {
            dataJson.remove("httpMethod")
        }
        dataObj = Requestmap.get(params["id"])
        dataObj.properties = dataJson
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
            springSecurityService.clearCachedRequestmaps()
        } else {
            result["status"] = false
            result["message"] = "Failed to Update Requestmap \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = Requestmap.get(params["id"])
        dataObj.delete(flush:true)
        springSecurityService.clearCachedRequestmaps()
        render result as JSON
    }
}
