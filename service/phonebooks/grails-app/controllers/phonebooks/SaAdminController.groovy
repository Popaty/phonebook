package phonebooks

import grails.converters.JSON
import grails.transaction.Transactional

class SaAdminController {

    static namespace = "v1"
    static allowedMethods = [countSaAdmin: "GET", create: "POST"]
    def errorMessageService
    def springSecurityService

    def countSaAdmin() {
        def result = [status: true]
        result["data"] = UserRole.countByRole(Role.findByAuthority("ROLE_SAADMIN"))
        render result as JSON
    }

    @Transactional
    def create() {
        def (result,dataJson) = [["status":true],request.JSON]
        def roleObj = Role.findByAuthority("ROLE_SAADMIN")
        def dataObj = new Users(dataJson)
        if (dataObj.save(flush: true)) {
            def urObj = new UserRole(dataObj,roleObj)
            if(urObj.save(flush: true))
            {
                def updateMapping = Requestmap.findByUrlAndHttpMethod("/saAdmins","POST")
                updateMapping.setConfigAttribute("ROLE_SAADMIN")
                if(updateMapping.save(flush: true))
                {
//                      Clear Cache
                    springSecurityService.clearCachedRequestmaps()
                    result["data"] = dataObj
                }else{
                    result["status"] = false
                    result["message"] = "Failed to Update Mapping /saAdmins"
                }
            }else{
                result["status"] = false
                result["message"] = "Failed to Save Role SaAdmin \n ${errorMessageService.serviceMethod(urObj.errors.allErrors)}"
            }
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Users \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}