package phonebooks

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'listmenus',
        description = 'Listmenus API V1',
        position = 6,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class ListmenuController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService

    @ApiOperation(value = 'List Listmenus',response=Listmenu, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'status',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    def list() {
        def result = [status: true]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"status"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
//        Data
        def dataObj = Listmenu.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("status","%${searchText}%")
            }
            order(fieldSort,sortValue)
        }
        result["data"] = resultsList
//        Count
        def dataTotal = Listmenu.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("status","%${searchText}%")
            }
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        render result as JSON
    }

    @ApiOperation(value = 'Get Listmenu data',response=Listmenu, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Listmenu id สำหรับดึงข้อมูล', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = Listmenu.get(params["id"])
        def result=[status: true]
        result["data"] = dataObj
        render result as JSON
    }

    @ApiOperation(value = 'Create Listmenu',response=Listmenu, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'status', value = 'ชื่อ status', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def (result,dataJson) = [["status":true],request.JSON]
        def dataObj = new Listmenu(dataJson)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Listmenu \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Update Listmenu', response = Listmenu, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Listmenu id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'body', paramType = 'body', dataType = 'string', required = true),
    ])
    @Transactional
    def update() {
        def (result, dataObj,dataJson) = [["status": true], null,request.JSON]
        dataObj = Listmenu.get(params["id"])
        dataObj.properties = dataJson
        if (dataObj.isDirty())
        {
            if (dataObj.save(flush: true)) {
                result["data"] = dataObj
            } else {
                result["status"] = false
                result["message"] = "Failed to Update Listmenu \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["data"] = dataObj
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete Listmenu', response = Listmenu, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Listmenu id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = Listmenu.get(params["id"])
        dataObj.delete(flush:true)
        render result as JSON
    }
}
