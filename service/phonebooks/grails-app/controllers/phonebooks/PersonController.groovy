package phonebooks

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'person',
        description = 'Person API V1',
        position = 8,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class PersonController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE",upload:"POST"]
    def errorMessageService
    def dataService

    @ApiOperation(value = 'List Person',response=UserAddcontact, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'userName',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'department', value = 'สังกัด', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'statusi', value = 'สถานะ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
    ])
    def list() {
        def result = [status: true]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"userName"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
//        Data
        def dataObj = UserAddcontact.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("userName","%${searchText}%")
                ilike("nikName","%${searchText}%")
                ilike("userMail","%${searchText}%")
                ilike("emailExt","%${searchText}%")
            }
            and {
                if(dataService.checkKey(params,"department"))
                {
                    eq("userDepart",Department.get(params["department"]))
                }
                if(dataService.checkKey(params,"statusi"))
                {
                    eq("statusi",params["statusi"])
                }
            }
            order(fieldSort,sortValue)
        }
        result["data"] = []
        resultsList.each {
            result["data"].push(convertPersonData(it))
        }
//        Count
        def dataTotal = UserAddcontact.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("userName","%${searchText}%")
                ilike("nikName","%${searchText}%")
                ilike("userMail","%${searchText}%")
                ilike("emailExt","%${searchText}%")
            }
            and {
                if(dataService.checkKey(params,"department"))
                {
                    eq("userDepart",Department.get(params["department"]))
                }
                if(dataService.checkKey(params,"statusi"))
                {
                    eq("statusi",params["statusi"])
                }
            }
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        render result as JSON
    }

    @ApiOperation(value = 'Get Person data',response=UserAddcontact, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'UserAddcontact id สำหรับดึงข้อมูล', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = UserAddcontact.get(params["id"])
        def result = [status: true]
        result["data"] = convertPersonData(dataObj)
        render result as JSON
    }

    def convertPersonData(UserAddcontact person)
    {
        def data= [id:person.getId()]
        data["idUser"] = person.getIdUser()
        data["conpass"] = person.getConpass()
        data["userName"] = person.getUserName()
        data["nikName"] = person.getNikName()
        data["floor"] = person.getFloor()
        data["userTel"] = person.getUserTel()
        data["mobile"] = person.getMobile()
        data["mobile2"] = person.getMobile2()
        data["fax"] = person.getMobile2()
        data["userMail"] = person.getUserMail()
        data["emailExt"] = person.getEmailExt()
        data["memo"] = person.getMemo()
        data["departmentName"] = person.getDepartmentName()
        data["deptId"] = person.getUserDepart().getId()
        data["statusiName"] = person.getStatusiName()
        data["statusi"] = person.getStatusi()
        data["status1"] = person.getStatus1()
        data["pic"] = person.getPic()?:""
        if(data["pic"] != "")
        {
            data["pic"] = uploadService.readImage(data["pic"],"profile/${data["id"]}")
        }
        return data
    }

    @ApiOperation(value = 'Create Person',response=UserAddcontact, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'conpass', value = '', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userName', value = 'ชื่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nikName', value = 'ชื่อเล่น', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userDepart', value = 'สังกัด', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'floor', value = 'ชั้น', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userTel', value = 'เบอร์ภายใน', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'mobile', value = 'เบอร์โทรศัพท์มือถือ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'mobile2', value = 'เบอร์โทรศัพท์มือถือ 2', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'fax', value = 'เบอร์โทรสาร', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userMail', value = 'อีเมล์ cccthai', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'emailExt', value = 'อีเมล์อื่นๆ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'pic', value = 'รูปภาพ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'memo', value = '', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'statusi', value = 'สถานะ', defaultValue = '1', paramType = 'query', dataType = 'integer', required=true),
            @ApiImplicitParam(name = 'status1', value = 'สิทธิ์การเข้าใช้', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        if(dataService.checkKey(params,"userDepart"))
        {
            params["userDepart"] = Department.get(params["userDepart"])
        }
        def dataObj = new UserAddcontact(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Person \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Update Person', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Person id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'conpass', value = '', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userName', value = 'ชื่อ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'nikName', value = 'ชื่อเล่น', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userDepart', value = 'สังกัด', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'floor', value = 'ชั้น', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userTel', value = 'เบอร์ภายใน', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'mobile', value = 'เบอร์โทรศัพท์มือถือ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'mobile2', value = 'เบอร์โทรศัพท์มือถือ 2', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'fax', value = 'เบอร์โทรสาร', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'userMail', value = 'อีเมล์ cccthai', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'emailExt', value = 'อีเมล์อื่นๆ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'pic', value = 'รูปภาพ', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'memo', value = '', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
            @ApiImplicitParam(name = 'statusi', value = 'สถานะ', defaultValue = '1', paramType = 'query', dataType = 'integer', required=true),
            @ApiImplicitParam(name = 'status1', value = 'สิทธิ์การเข้าใช้', defaultValue = '', paramType = 'query', dataType = 'string', required=false),
    ])
    @Transactional
    def update() {
        def result = ["status": true]
        if(dataService.checkKey(params,"userDepart"))
        {
            params["userDepart"] = Department.get(params["userDepart"])
        }
        def dataObj = UserAddcontact.get(params["id"])
        dataObj.properties = params
        if (dataObj.isDirty())
        {
            if (dataObj.save(flush: true)) {
                result["data"] = dataObj
            } else {
                result["status"] = false
                result["message"] = "Failed to Update Person \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["data"] = dataObj
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete Person', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Person id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = UserAddcontact.get(params["id"])
        dataObj.delete(flush:true)
        render result as JSON
    }

    def uploadService
    @ApiOperation(value = 'Upload File', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = '', paramType = 'form', dataType = 'path', required = false),
            @ApiImplicitParam(name = 'pic', value = '', paramType = 'form', dataType = 'file', required = false),
    ])
    @Transactional
    def upload() {
        def (result, dataObj, fileName) = [[status: true], UserAddcontact.get(params["id"]), ""]
        def folderPath = "/profile/${params["id"]}"
        if (dataService.checkKey(params, "pic")) {
            if (params["pic"] == "delete") {
                uploadService.deleteFile(dataObj.getPic(), folderPath)
                dataObj.setPic("")
            } else {
                if (dataObj.getPic() != "") {
                    uploadService.deleteFile(dataObj.getPic(), folderPath)
                }
                fileName = uploadService.uploadFile(request, "pic", folderPath, "profile_${params["id"]}")
                dataObj.setPic(fileName)
            }
        }
        if(!dataObj.save(flush:true))
        {
            result["status"] = false
            result["message"] = "Failed to Upload Personnel Image \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }
}
