package phonebooks


import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'roles',
        description = 'Role API ( Role สำหรับ login ) V1',
        position = 2,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class RoleController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def springSecurityService
    def errorMessageService

    def list() {
        def authority = springSecurityService.getPrincipal().getAuthorities()[0].getAuthority()
        def (result,resultArray) = [[:],[]]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"id"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
        def rObj = Role.createCriteria()
        def resultsList = rObj.list (max: max, offset: offset) {
            or{
                ilike("authority","%${searchText}%")
            }
            if (authority == "ROLE_ADMIN")
            {
                and {
                    ne("authority","ROLE_SAADMIN")
                }
            }
            order(fieldSort,sortValue)
        }
        def rTotal = Role.createCriteria()
        def totalRecord = rTotal.list {
            projections {
                count()
            }
            or{
                ilike("authority","%${searchText}%")
            }
            if (authority == "ROLE_ADMIN")
            {
                and {
                    ne("authority","ROLE_SAADMIN")
                }
            }
        }
        resultsList.each {
            def item = [:]
            item["id"] = it.getId()
            item["authority"] = it.getAuthority()
            resultArray.push(item)
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        result["data"] = resultArray
        result["status"] = true
        render result as JSON
    }

    def show() {
        def (roleObj,result) = [Role.get(params["id"]),[status:true]]
        result["data"] = roleObj
        render result as JSON
    }

    @Transactional
    def create() {
        def (result,dataJson) = [["status":true],request.JSON]
        def dataObj = new Role(dataJson)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Role \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def update() {
        def (result, dataObj,dataJson) = [["status": true], null,request.JSON]
        dataObj = Role.get(params["id"])
        dataObj.properties = dataJson
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Update Role \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = Role.get(params["id"])
        def countUser = UserRole.countByRole(dataObj)
        if (countUser > 0)
        {
            result["status"] = false
            result["message"] = "Have User use this Role"
        }else{
            dataObj.delete(flush:true)
        }
        render result as JSON
    }
}