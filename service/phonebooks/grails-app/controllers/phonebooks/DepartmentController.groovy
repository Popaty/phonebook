package phonebooks

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.sql.Sql

@Api(
        value = 'departments',
        description = 'Departments API ( สังกัด ) V1',
        position = 5,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class DepartmentController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE",people:"GET"]
    def errorMessageService

    @ApiOperation(value = 'List Departments',response=Department, responseContainer = 'list',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'searchText', value = 'Text for search', defaultValue = '',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'currentPage', value = 'Current Page', defaultValue = '1',required=false, paramType = 'query', dataType = 'int'),
            @ApiImplicitParam(name = 'fieldSort', value = 'Field for Sorting', defaultValue = 'deptName',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'sortValue', value = 'Sorting value', defaultValue = 'asc',required=false, paramType = 'query', dataType = 'string'),
            @ApiImplicitParam(name = 'max', value = 'Max value,0 = all', defaultValue = '20',required=false, paramType = 'query', dataType = 'int'),
    ])
    def list() {
        def result = [status: true]
        def searchText = params["searchText"]?:""
        def currentPage = params["currentPage"]?params["currentPage"].toInteger():1
        def fieldSort = params["fieldSort"]?:"deptName"
        def sortValue = params["sortValue"]?:"asc"
        def max = params["max"]?params["max"].toInteger():20
        def offset = (currentPage - 1) * max
//        Data
        def dataObj = Department.createCriteria()
        def resultsList = dataObj.list (max: max, offset: offset) {
            or{
                ilike("deptName","%${searchText}%")
            }
            order(fieldSort,sortValue)
        }
        result["data"] = resultsList
//        Count
        def dataTotal = Department.createCriteria()
        def totalRecord = dataTotal.list {
            projections {
                count()
            }
            or{
                ilike("deptName","%${searchText}%")
            }
        }
        if(max == 0)
        {
            result["totalPage"] = 1
        }else{
            result["totalPage"] = Math.ceil(totalRecord[0] / max)
        }
        render result as JSON
    }

    @ApiOperation(value = 'Get Department data',response=Department, responseContainer = 'json',produces='application/json',httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Department id สำหรับดึงข้อมูล', paramType = 'path', dataType = 'string',required=true),
    ])
    def show() {
        def dataObj = Department.get(params["id"])
        def result=[status: true]
        result["data"] = dataObj
        render result as JSON
    }

    @ApiOperation(value = 'Create Department',response=Department, responseContainer = 'json',produces='application/json',httpMethod = 'POST')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'deptName', value = 'ชื่อ Department', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
    ])
    @Transactional
    def create() {
        def result = ["status":true]
        def dataObj = new Department(params)
        if (dataObj.save(flush: true)) {
            result["data"] = dataObj
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Department \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @ApiOperation(value = 'Update Department', response = Department, responseContainer = 'json', produces = 'application/json', httpMethod = 'PUT')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Department id สำหรับ update', defaultValue = '', paramType = 'path', dataType = 'string', required = true),
            @ApiImplicitParam(name = 'deptName', value = 'ชื่อ Department', defaultValue = '', paramType = 'query', dataType = 'string', required=true),
//            @ApiImplicitParam(name = 'body', paramType = 'body', dataType = 'string', required = true),
    ])
    @Transactional
    def update() {
        def (result, dataObj) = [["status": true], null]
        dataObj = Department.get(params["id"])
        dataObj.properties = params
        if (dataObj.isDirty())
        {
            if (dataObj.save(flush: true)) {
                result["data"] = dataObj
            } else {
                result["status"] = false
                result["message"] = "Failed to Update Department \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
            }
        }else{
            result["data"] = dataObj
        }
        render result as JSON
    }

    @ApiOperation(value = 'Delete Department', response = Department, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Department id สำหรับ delete', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = Department.get(params["id"])
        if(dataObj.getPeopleCount() > 0)
        {
            result["status"] = false
            result["message"] = "ไม่สามารถลบข้อมูลได้ มีการใช้ข้อมูลนี้อยู่"
        }else{
            dataObj.delete(flush:true)
        }

        render result as JSON
    }

    @ApiOperation(value = 'List person in department', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'GET')
    @ApiImplicitParams([
            @ApiImplicitParam(name = 'id', value = 'Department id สำหรับ filter', paramType = 'path', dataType = 'string', required = true),
    ])
    @Transactional
    def people() {
        def result = [status:true]
        result["data"] = UserAddcontact.findAllByUserDepart(Department.get(params["id"]),[sort:"userName",order:"asc"])
        render result as JSON
    }
}
