package phonebooks

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional

@Api(
        value = 'report',
        description = 'Report API V1',
        position = 9,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class ReportController {

    static namespace = "v1"
    static allowedMethods = [sumbydepartment: "GET",active: "GET", inactive: "GET"]
    def errorMessageService

    @ApiOperation(value = 'List person in department', response = Department, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @Transactional
    def sumbydepartment() {
        def result = [status:true]
        def depList = Department.findAll([sort:"deptName",order:"asc"])
        result["data"] = []
        depList.each {
            def data = [departmentName:it.getDeptName()]
            data["count"] = it.getPeopleCount()
            result["data"].push(data)
        }
        render result as JSON
    }

    @ApiOperation(value = 'List person in active', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @Transactional
    def active() {
        def result = [status:true]
        result["data"] = UserAddcontact.findAllByStatusi(1,[sort:"userName",order:"asc"])
        render result as JSON
    }

    @ApiOperation(value = 'List person in inactive', response = UserAddcontact, responseContainer = 'json', produces = 'application/json', httpMethod = 'DELETE')
    @Transactional
    def inactive() {
        def result = [status:true]
        result["data"] = UserAddcontact.findAllByStatusi(2,[sort:"userName",order:"asc"])
        render result as JSON
    }
}
