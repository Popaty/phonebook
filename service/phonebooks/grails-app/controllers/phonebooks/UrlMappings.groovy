package phonebooks

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')

        group "/users", {
            "/list"(version:'1.0', controller: "user", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "user" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "user" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "user" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "user" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/saAdmins", {
            "/"(version:'1.0', controller: "saAdmin" , action: "countSaAdmin", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "saAdmin" , action: "create", method:"POST", namespace:'v1')
        }

        group "/roles", {
            "/list"(version:'1.0', controller: "role", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "role" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "role" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "role" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "role" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/requestmaps", {
            "/list"(version:'1.0', controller: "requestmap", action: "list", method:"GET", namespace:'v1')
            "/loadDefault"(version:'1.0', controller: "requestmap", action: "loadDefault", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "requestmap" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "requestmap" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "requestmap" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "requestmap" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/api/department", {
            "/list"(version:'1.0', controller: "department", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "department" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "department" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "department" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "department" , action: "delete", method:"DELETE", namespace:'v1')
            "/$id/people"(version:'1.0', controller: "department" , action: "people", method:"GET", namespace:'v1')
        }

        group "/api/listmenus", {
            "/list"(version:'1.0', controller: "listmenu", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "listmenu" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "listmenu" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "listmenu" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "listmenu" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/api/modules", {
            "/list"(version:'1.0', controller: "module", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "module" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "module" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "module" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "module" , action: "delete", method:"DELETE", namespace:'v1')
        }

        group "/api/person", {
            "/list"(version:'1.0', controller: "person", action: "list", method:"GET", namespace:'v1')
            "/"(version:'1.0', controller: "person" , action: "create", method:"POST", namespace:'v1')
            "/$id"(version:'1.0', controller: "person" , action: "show", method:"GET", namespace:'v1')
            "/$id"(version:'1.0', controller: "person" , action: "update", method:"PUT", namespace:'v1')
            "/$id"(version:'1.0', controller: "person" , action: "delete", method:"DELETE", namespace:'v1')
            "/$id/upload"(version:'1.0', controller: "person" , action: "upload", method:"POST", namespace:'v1')
        }

        group "/api/report", {
            "/sumbydepartment"(version:'1.0', controller: "report", action: "sumbydepartment", method:"GET", namespace:'v1')
            "/person/active"(version:'1.0', controller: "report" , action: "active", method:"GET", namespace:'v1')
            "/person/inactive"(version:'1.0', controller: "report" , action: "inactive", method:"GET", namespace:'v1')
        }
    }
}
