package phonebooks

import com.wordnik.swagger.annotations.Api
import com.wordnik.swagger.annotations.ApiImplicitParam
import com.wordnik.swagger.annotations.ApiImplicitParams
import com.wordnik.swagger.annotations.ApiOperation
import grails.converters.JSON
import grails.transaction.Transactional
import groovy.sql.Sql

@Api(
        value = 'users',
        description = 'Users API ( user สำหรับ login ) V1',
        position = 1,
        produces = 'application/json',
        consumes = 'application/json,application/x-www-form-urlencoded'
)
class UserController {

    static namespace = "v1"
    static allowedMethods = [list: "GET",show: "GET", create: "POST", update:"PUT",delete:"DELETE"]
    def errorMessageService
    def springSecurityService
    def dataSource

    def list() {
        def result = [status: true]
        def db = new Sql(dataSource)
        def searchText = "LOWER('%${params["searchTxt"]?:""}%')"
        String query = $/
            SELECT users.id,users.username,users.first_name,users.last_name FROM users JOIN
            user_role ON users.id = user_role.user_id
            WHERE
                user_role.role_id = ${params["role"].toInteger()} AND
            (
                LOWER(users.username) like ${searchText} OR
                LOWER(users.first_name) like ${searchText} OR
                LOWER(users.last_name) like ${searchText}
            )
            ORDER BY users.id ASC
        /$
        def userList = db.rows(query)
        result["data"] = []
        for (item in userList)
        {
            def data = [id:item["id"]]
            data["username"] = item["username"]
            data["fullName"] = "${item["first_name"]} ${item["last_name"]}"
            result["data"].push(data)
        }
        render result as JSON
    }

    def show() {
        def dataObj = Users.get(params["id"])
        def (data,result)=[[id:dataObj.getId()],[status: true]]
        data["username"] = dataObj.getUsername()
        data["firstName"] = dataObj.getFirstName()
        data["lastName"] = dataObj.getLastName()
        data["email"] = dataObj.getEmail()
//        data["password"] = dataObj.getPassword()
        def authorities = dataObj.getAuthorities()
        if (authorities.size() > 0)
        {
            data["role"] = authorities[0]["id"]
            data["roleName"] = authorities[0]["authority"]
        }else{
            data["role"] = ""
            data["roleName"] = ""
        }
        result["data"] = data
        render result as JSON
    }

    @Transactional
    def create() {
        def (result,dataJson) = [["status":true],request.JSON]
        def dataObj = new Users(dataJson)
        if (dataObj.save(flush: true)) {
            def urObj = new UserRole(dataObj,Role.get(dataJson["role"]))
            if(urObj.save(flush: true))
            {
                result["data"] = dataObj
            }else{
                result["status"] = false
                result["message"] = "Failed to Save Role \n ${errorMessageService.serviceMethod(urObj.errors.allErrors)}"
            }
        } else {
            result["status"] = false
            result["message"] = "Failed to Save Users \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def update() {
        def (result, comObj, dataObj,dataJson) = [["status": true], null, null,request.JSON]
        dataObj = Users.get(params["id"])
        dataObj.properties = dataJson
        if (dataObj.save(flush: true)) {
            UserRole.removeAll(dataObj,true)
            def urObj = new UserRole(dataObj,Role.get(dataJson["role"]))
            if(urObj.save(flush: true))
            {
                result["data"] = dataObj
            }else{
                result["status"] = false
                result["message"] = "Failed to Update Role \n ${errorMessageService.serviceMethod(urObj.errors.allErrors)}"
            }
        } else {
            result["status"] = false
            result["message"] = "Failed to Update Users \n ${errorMessageService.serviceMethod(dataObj.errors.allErrors)}"
        }
        render result as JSON
    }

    @Transactional
    def delete() {
        def result = [:]
        result["status"] = true
        def dataObj = Users.get(params["id"])
        UserRole.removeAll(dataObj,true)
        dataObj.delete(flush:true)
        render result as JSON
    }
}