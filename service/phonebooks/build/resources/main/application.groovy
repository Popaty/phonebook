grails.reload.enabled = true
grails.plugin.databasemigration.updateOnStart = true
grails.json.legacy.builder = true
swaggydoc {
    contact = "rahul.som@gmail.com"
    description = """\
        | This is a sample server Petstore server.  You can find out more about Swagger
        | at <a href="http://swagger.wordnik.com">http://swagger.wordnik.com</a> or on irc.freenode.net, #swagger.
        | For this sample,
        | you can use the api key "special-key" to test the authorization filters""".stripMargin()
    license = "Apache 2.0"
    licenseUrl = "http://www.apache.org/licenses/LICENSE-2.0.html"
    termsOfServiceUrl = "http://helloreverb.com/terms/"
    title = "Swaggydoc"
    apiVersion = "1.0"
}
grails.plugin.springsecurity.userLookup.userDomainClassName = 'phonebooks.Users'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'phonebooks.UserRole'
grails.plugin.springsecurity.authority.className = 'phonebooks.Role'
grails.plugin.springsecurity.requestMap.className = "phonebooks.Requestmap"
grails.plugin.springsecurity.securityConfigType = "Requestmap"
grails.plugin.springsecurity.logout.postOnly = false
grails.plugin.springsecurity.rest.logout.endpointUrl = '/api/logout'
grails.plugin.springsecurity.rest.login.failureStatusCode = 402
grails.plugin.springsecurity.rest.token.validation.useBearerToken = false
grails.plugin.springsecurity.rest.token.validation.headerName = 'Authorization'